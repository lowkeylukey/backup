import os, os.path, sys, shutil, pickle, contentHash, logger, time

#Define archive location
archive = os.path.expanduser("~/Desktop/myArchive")

#Define objects and index
objects = os.path.join(archive, "objects")
index = os.path.join(archive, "index")

def init():
    #If archive does not exist, create it
    if not os.path.exists(archive) or not os.path.isdir(archive):
        os.mkdir(archive)

    #If objects does not exist, create it
    if not os.path.exists(objects) or not os.path.isdir(objects):
        os.mkdir(objects)

    #If index does not exist, create it
    if not os.path.exists(index) or not os.path.isfile(index):
        indexFile = open(index, "w")
        emptyDict = {}
        pickle.dump(emptyDict, indexFile)
        indexFile.close()

def store(directory):
    #First, check to make sure objects and index exists
    if not os.path.exists(objects) and not os.path.isdir(objects):
        print "You must first call init."
        return
    if not os.path.exists(index):
        print "You must first call init."
        return

    #Check to make sure the given directory exists
    if not os.path.exists(directory):
        print "No such directory exists."
    elif not os.path.isdir(directory):
        print "This is not a directory."
    else:
        #Update log file
        logger.logger.info('Store was run on ' + time.ctime())
        
        #If directory does exist
        #Load index
        indexFile = open(index, "r")
        indexContents = pickle.load(indexFile)
        indexFile.close()

        #Create a list with the hash data of the directory files
        (head, tail) = os.path.split(directory)
        hashDetails = []
        hashDetails = processDirectory(directory, hashDetails, tail)

        filesAdded = ""
        notAddedCount = 0

        for detail in hashDetails:
            #Check to see if the filename already exists in archive
            if detail[3] in indexContents:
                #Check to see if the file has the same content
                if indexContents[detail[3]] == detail[2]:
                    #If same content, don't need to add to archive
                    notAddedCount = notAddedCount + 1
                else:
                    #If different content, file needs to be updated
                    #Delete old file
                    os.remove(os.path.join(objects, indexContents[detail[3]]))
                    #Copy new file to archive and update index
                    shutil.copyfile(detail[0], os.path.join(objects, detail[2]))
                    indexContents[detail[3]] = detail[2]
                    filesAdded = filesAdded + "\n" + detail[3]
                    #Update log file
                    logger.logger.info(detail[0] + ' was added to the archive on ' + time.ctime())

            else:
                #If filename doesn't exist in archive, check if file does but under a different name (ie same contents)
                if detail[2] in indexContents.values():
                    #If it does, no need to copy it
                    notAddedCount = notAddedCount + 1
                    #Still need to update index
                    indexContents[detail[3]] = detail[2]
                    #Update log file
                    logger.logger.info(detail[0] + ' was added to the archive on ' + time.ctime())
                else:
                    #If file does not exist, need to copy it and update index
                    shutil.copyfile(detail[0], os.path.join(objects, detail[2]))
                    indexContents[detail[3]] = detail[2]
                    filesAdded = filesAdded + "\n" + detail[3]
                    #Update log file
                    logger.logger.info(detail[0] + ' was added to the archive on ' + time.ctime())
                    
        #Save index
        indexFile = open(index, "w")
        pickle.dump(indexContents, indexFile)
        indexFile.close()

        #Print what was added
        if filesAdded == "":
            print "No files were added to the archive."
        else:
            print "The following files were added to the archive:" + filesAdded
        print "There were %d files already in the archive." % notAddedCount

def test():
    #Ensure objects and index exist
    if not os.path.exists(index) or not os.path.exists(objects) or not os.path.isdir(objects):
        print "The archive does not exist or is corrupted."
        return

    correctEntries = 0
    correctContent = 0
    errors = ""
    #Get index contents
    indexFile = open(index, "r")
    indexContents = pickle.load(indexFile)
    indexFile.close()

    #Check that objects listed in index exist in the objects directory
    keys = indexContents.keys()
    for key in keys:
        if os.path.exists(os.path.join(objects, indexContents[key])):
            correctEntries = correctEntries + 1
        else:
            errors = errors + "\n\t" + key + " was not found in the objects directory."

    #Check that files in "objects" have the correct content
    files = os.listdir(objects)
    for file in files:
        hash = contentHash.createFileSignature(os.path.join(objects, file))[2]
        if hash == file:
            correctContent = correctContent + 1
        else:
            errors = errors + "\n\t" + "the content of " + file + " is not correct."
        
    print "Number of correct entries in the index file: %i" % correctEntries
    print "Number of files that have the correct content: %i" % correctContent
    if not errors == "":
        print "The following errors were found:" + errors

    #Update log file
    logger.logger.info('Test was run on ' + time.ctime() + ' with the following results:')
    logger.logger.info("Number of correct entries in the index file: %i" % correctEntries)
    logger.logger.info("Number of files that have the correct content: %i" % correctContent)
    if not errors == "":
        logger.logger.info("The following errors were found:" + errors)


def list(pattern):
    #Checks if the archive exists, and returns if it doesn't
    if not os.path.exists(index):
        print "The Archive does not exist or is corrupted."
        return
    
    #Gets the keys(file names) from the index file
    indexFile = open(index, "r")
    indexContent = pickle.load(indexFile)
    indexFile.close()
    files = indexContent.keys()
    
    #Checks if there are any files in the archive, if they need to match 
    #a specific pattern, and prints all relevent files.
    if len(files) == 0:
        print "The archive contains no files."
        return
    elif pattern == "":
        for file in files:
            print file
        return
    else:
        matchedFiles = ""
        for file in files:
            if pattern in file:
                matchedFiles = matchedFiles + "\n" + file
        if matchedFiles == "":
            print "No files in the archive match the search."
            return
        else:
            print "The following files in the archive match the search: " + matchedFiles
            return

def get(filename):
    #Check that the archive has been created.
    if not os.path.exists(index) or not os.path.exists(objects) or not os.path.isdir(objects):
        print "The archive does not exist or is corrupted."
        return
    
    #Open the index and get the list of files.
    indexFile = open(index, "r")
    indexContent = pickle.load(indexFile)
    indexFile.close()
    
    #Check if the filename exists and then copy to the current directory if it does.
    if filename in indexContent.keys():
        if os.path.exists(os.path.join(os.getcwd(), os.path.basename(filename))):
            while True:            
                overwrite = raw_input("A file with this name already exists in the current directory. Do you want to overwrite this file(y or n)? ")
                if overwrite == "n":
                    return
                elif overwrite == "y":
                    print "Copying..."
                    shutil.copyfile(os.path.join(objects, indexContent[filename]), os.path.join(os.getcwd(), os.path.basename(filename)))
                    print "Completed."                    
                    return
                else:
                    print overwrite + " is not a valid option."
        else:
            print "Copying..."
            shutil.copyfile(os.path.join(objects, indexContent[filename]), os.path.join(os.getcwd(), os.path.basename(filename)))
            print "Completed."            
            return
    else:
        print filename + " is not in the archive."
        return
        
def restore(directory):
    #Check the archive has been created.
    if not os.path.exists(index) or not os.path.exists(objects) or not os.path.isdir(objects):
        print "The archive does not exist or is corrupted."
        return
    
    #Open the index and get the list of files.
    indexFile = open(index, "r")
    indexContent = pickle.load(indexFile)
    indexFile.close()
    
    #Copy all the files to the new directory
    print "Restoring all files..."
    for key in indexContent.keys():
        (path, filename) = os.path.split(key)
        savePath = os.path.join(directory, path)
        #If the directory does not exist, create it.
        if not os.path.exists(savePath) or not os.path.isdir(savePath):
            os.makedirs(savePath)
        shutil.copyfile(os.path.join(objects, indexContent[key]), os.path.join(directory, key))
    print "completed."

#Creates a list of tuples (containing the pathname, last modification time, SHA1 hash, and base directory) of each file in the directory      
def processDirectory(directory, processedFiles, baseDirectory):
    files = os.listdir(directory)
    for file in files:
        if os.path.isdir(os.path.join(directory, file)):
            processedFiles = processDirectory(os.path.join(directory, file), processedFiles, os.path.join(baseDirectory, file))
        else:
            processedFiles.append(contentHash.createFileSignature(os.path.join(directory, file)) + (os.path.join(baseDirectory, file),))
    return processedFiles


if sys.argv[1] == "init":
    init()
elif sys.argv[1] == "store":
    if len(sys.argv) > 2:
        store(sys.argv[2])
    else:
        print "You need to provide a directory to store."
elif sys.argv[1] == "list":
    if len(sys.argv) > 2:
        list(sys.argv[2])
    else:
        list("")
elif sys.argv[1] == "get":
    if len(sys.argv) > 2:
        get(sys.argv[2])
    else:
        print "No filename specified."
elif sys.argv[1] == "restore":
    if len(sys.argv) > 2:
        restore(sys.argv[2])
    else:
        print "No restore directory specified."
elif sys.argv[1] == "test":
    test()
else:
    print "Command not recognised."